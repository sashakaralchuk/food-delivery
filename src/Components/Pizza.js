import React, { Component } from 'react';
import Request from 'superagent';

class Pizza extends Component {

    state = {
        pizza: {}
    }

    componentWillMount() {
        Request.get(`http://localhost:3002/pizza/${this.props.match.params.name}`)
            .then(response => {
                this.setState({
                    pizza: JSON.parse(response.text)[0]
                })
            });

    }

    render() {
        return (
            <div>
                <div>{this.state.pizza.title}</div>
                <div>{this.state.pizza.weight}</div>
                <div>{this.state.pizza.price}</div>
                <div>{this.state.pizza.diameterCM}</div>
            </div>
        );
    }
}

export default Pizza;