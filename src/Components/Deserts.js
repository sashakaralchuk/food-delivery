import React, { Component } from 'react';
import { connect } from 'react-redux';
import Request from 'superagent';
import {Link} from 'react-router-dom';


class Deserts extends Component {

    componentWillMount() {
        this.props.onGetDeserts();
    }

    render() {
        return (
            <ul>
                {this.props.deserts.map((desert, index) => {
                    const link = "/desert/" + desert.title;
                    return <li key={index}>
                        <Link to={link}>{desert.title}</Link>
                    </li>
                })}
            </ul>
        );
    }
}

export default connect(
    state => ({
        deserts: state.deserts
    }),
    dispatch => ({
        onGetDeserts: () => {
            Request.get('http://localhost:3002/deserts')
                .then(response => {
                    dispatch({
                        type: "GET_DESERTS",
                        payload: response.body
                    })
                });
        }
    })
)(Deserts);