import React, { Component } from 'react';
import Request from 'superagent';

class Drink extends Component {

    state = {
        drink: {}
    }

    componentWillMount() {
        Request.get(`http://localhost:3002/drink/${this.props.match.params.name}`)
            .then(response => {
                this.setState({
                    drink: JSON.parse(response.text)[0]
                })
            });

    }

    render() {
        return (
            <div>
                <div>{this.state.drink.title}</div>
                <div>{this.state.drink.price}</div>
                <div>{this.state.drink.volume}</div>
            </div>
        );
    }
}

export default Drink;