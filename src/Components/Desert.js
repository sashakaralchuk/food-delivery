import React, { Component } from 'react';
import Request from 'superagent';

class Desert extends Component {

    state = {
        desert: {}
    }

    componentWillMount() {
        Request.get(`http://localhost:3002/desert/${this.props.match.params.name}`)
            .then(response => {
                this.setState({
                    desert: JSON.parse(response.text)[0]
                })
            });
    }

    render() {
        return (
            <div>
                <div>{this.state.desert.title}</div>
                <div>{this.state.desert.price}</div>
                <div>{this.state.desert.weight}</div>
            </div>
        );
    }
}

export default Desert;