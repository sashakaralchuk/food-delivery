import React, { Component } from 'react';
import { connect } from 'react-redux';
import Request from 'superagent';
import {Link} from 'react-router-dom';


class Pizzas extends Component {

    componentWillMount() {
        this.props.onGetPizzas();
    }

    render() {
        return (
            <ul>
                {this.props.pizzas.map((pizza, index) => {
                    const link = "/pizza/" + pizza.title;
                    return <li key={index}>
                        <Link to={link}>{pizza.title}</Link>
                    </li>
                })}
            </ul>
        );
    }
}

export default connect(
    state => ({
        pizzas: state.pizzas
    }),
    dispatch => ({
        onGetPizzas: () => {
            Request.get('http://localhost:3002/pizzas')
                .then(response => {
                    dispatch({
                        type: "GET_PIZZAS",
                        payload: response.body
                    })
                });
        }
    })
)(Pizzas);