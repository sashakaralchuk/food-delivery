import React, { Component } from 'react';
import { connect } from 'react-redux';
import Request from 'superagent';
import {Link} from 'react-router-dom';


class Drinks extends Component {

    componentWillMount() {
        this.props.onGetDrinks();
    }

    render() {
        return (
            <ul>
                {this.props.drinks.map((drink, index) => {
                    const link = "/drink/" + drink.title;
                    return <li key={index}>
                        <Link to={link}>{drink.title}</Link>
                    </li>
                })}
            </ul>
        );
    }
}

export default connect(
    state => ({
        drinks: state.drinks
    }),
    dispatch => ({
        onGetDrinks: () => {
            Request.get('http://localhost:3002/drinks')
                .then(response => {
                    dispatch({
                        type: "GET_DRINKS",
                        payload: response.body
                    })
                });
        }
    })
)(Drinks);