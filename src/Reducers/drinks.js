const initialState = [];

export default function drinks(state = initialState, action) {
    switch (action.type) {
        case 'GET_DRINKS':
            return action.payload;
        default:
            return state;
    }
}