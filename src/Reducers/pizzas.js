const initialState = [];

// reducer
export default function pizzas(state = initialState, action) {
    switch (action.type) {
        case 'GET_PIZZAS':
            return action.payload;
        default:
            return state;
    }
}