const initialState = [];

export default function deserts(state = initialState, action) {
    switch (action.type) {
        case 'GET_DESERTS':
            return action.payload;
        default:
            return state;
    }
}