import { combineReducers } from 'redux';
import pizzas from './pizzas';
import drinks from './drinks';
import deserts from './deserts';

export default combineReducers({
    deserts,
    pizzas,
    drinks
});