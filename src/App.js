import React, { Component } from 'react';
import Home from "./Components/Home";
import Drinks from "./Components/Drinks";
import Pizzas from "./Components/Pizzas";
import Deserts from "./Components/Deserts";
import Pizza from './Components/Pizza';
import Drink from "./Components/Drink";
import Desert from "./Components/Desert";

import {Route, Link} from 'react-router-dom';



class App extends Component {
  render() {
    return (
        <div>
            <div><Link to="/Home">Home</Link></div>
            <div><Link to="/Deserts">deserts</Link></div>
            <div><Link to="/Pizzas">Pizzas</Link></div>
            <div><Link to="/Drinks">Drinks</Link></div>
            <hr/>

            <Route exact path="/Home" component={Home} />
            <Route exact path="/Deserts" component={Deserts} />
            <Route exact path="/Pizzas" component={Pizzas} />
            <Route exact path="/Drinks" component={Drinks} />
            <Route path="/pizza/:name" component={Pizza} />
            <Route path="/drink/:name" component={Drink} />
            <Route path="/desert/:name" component={Desert} />
        </div>
    );
  }
}

export default App;
